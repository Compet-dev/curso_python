# Curso de Python 3

O curso de `Python` é uma atividade pertecente ao `COMPET` que possui como objetivo o aprendizado da linguagem de programação de uma maneira interativa e diferente, não focando simplesmente na oferta de conteúdo técnico da linguagem, mas sim na sua utilização para a resolução de grandes `desafios`, normalmente orientados as areas de conhecimento dos alunos.

## Informações

Data da primeira aula: `16/09/19`

Horário proposto: `15:40` às `17:30`

Local: Laboratório `12` do prédio `12`

Recursos: Projetor `7`